import socketIOClient from 'socket.io-client'
import sailsIOClient from 'sails.io.js'

var io = sailsIOClient(socketIOClient)
io.sails.url = process.env.API_BASE_URL

export default io
