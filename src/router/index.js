import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/components/Index'
import Page1 from '@/components/Page1'
import Page2 from '@/components/Page2'
import Echanger from '@/components/Echanger'
import team from '@/components/team'
import maps from '@/components/maps'
import Guide from '@/components/Guide'
import pecheur from '@/components/pecheur'
import chasseur from '@/components/chasseur'
import guerisseur from '@/components/guerisseur'
import batisseur from '@/components/batisseur'
import agriculteur from '@/components/agriculteur'
import mineur from '@/components/mineur'
import Discussion from '@/components/Discussion'
import Slider from '@/components/slider'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index
    },
    {
      path: '/page1',
      name: 'Page1',
      component: Page1
    },
    {
      path: '/page2',
      name: 'Page2',
      component: Page2
    },
    {
      path: '/echanger',
      name: 'Echanger',
      component: Echanger
    },
    {
      path: '/team',
      name: 'team',
      component: team
    },
    {
      path: '/maps',
      name: 'Maps',
      component: maps
    },
    {
      path: '/guide',
      name: 'Guide',
      component: Guide
    },
    {
      path: '/discussion',
      name: 'Discussion',
      component: Discussion
    },
    {
      path: '/slider',
      name: 'Slider',
      component: Slider
    },
    {
      path: '/pecheur',
      name: 'Pecheur',
      component: pecheur
    },
    {
      path: '/mineur',
      name: 'mineur',
      component: mineur
    },
    {
      path: '/agriculteur',
      name: 'agriculteur',
      component: agriculteur
    },
    {
      path: '/batisseur',
      name: 'batisseur',
      component: batisseur
    },
    {
      path: '/guerisseur',
      name: 'guerisseur',
      component: guerisseur
    },
    {
      path: '/chasseur',
      name: 'chasseur',
      component: chasseur
    }
  ]
})
