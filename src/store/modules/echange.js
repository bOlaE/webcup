import io from '../../socket.io'

const state = {
}
const getters = {
}

const actions = {
  getProduit ({ commit }) {
    return new Promise((resolve, reject) => {
      io.socket.get('/produits', function (response) {
        console.log('RESPONSE', response)
        if (response) {
          return resolve(response)
        } else {
          return reject(response)
        }
      })
    })
  },
  exchange ({ commit }, param) {
    return new Promise((resolve, reject) => {
      io.socket.post('/trocs', param, function (response) {
        console.log('RESPONSE TROCS', response)
        if (response) {
          return resolve(response)
        } else {
          return reject(response)
        }
      })
    })
  }
}

const mutations = {
}

export default {
  state,
  getters,
  actions,
  mutations
}
