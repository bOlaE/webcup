import io from '../../socket.io'

const state = {
}
const getters = {
}

const actions = {
  getComments ({ commit }) {
    return new Promise((resolve, reject) => {
      io.socket.get('/commentaires', function (response) {
        console.log('RESPONSE', response)
        if (response) {
          return resolve(response)
        } else {
          return reject(response)
        }
      })
    })
  },
  addCommentaire ({ commit }, param) {
    return new Promise((resolve, reject) => {
      io.socket.post('/commentaires', param, function (response) {
        console.log('RESPONSE TROCS', response)
        if (response) {
          return resolve(response)
        } else {
          return reject(response)
        }
      })
    })
  }
}

const mutations = {
}

export default {
  state,
  getters,
  actions,
  mutations
}
