import Vue from 'vue'
import Vuex from 'vuex'
import echange from './modules/echange'
import comment from './modules/comment'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    echange,
    comment
  },
  strict: true
})
