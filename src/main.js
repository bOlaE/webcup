// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import M from 'materialize-css'
import store from './store'
import VueParticles from 'vue-particles'
import 'animate.css'
import VueDirectionHover from 'vue-direction-hover'
import vSelect from 'vue-select'
import 'ammap3/ammap/ammap'
import 'ammap3/ammap/themes/dark'
import 'ammap3/ammap/themes/light'
import 'ammap3/ammap/maps/js/worldLow'
import 'amcharts3/amcharts/serial'
import 'lightbox2/dist/css/lightbox.css'
import 'izitoast/dist/css/iziToast.css'
import 'izitoast/dist/js/iziToast'
import Vuelidate from 'vuelidate'

Vue.use(VueDirectionHover)

require('font-awesome/css/font-awesome.min.css')
require('ammap3/ammap/ammap.css')
require('font-awesome/css/font-awesome.min.css')
require('typeface-montserrat')

Vue.use(Vuelidate)
Vue.component('v-select', vSelect)
Vue.use(VueDirectionHover)
Vue.config.productionTip = false
Vue.prototype.$M = M

Vue.use(VueParticles)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
